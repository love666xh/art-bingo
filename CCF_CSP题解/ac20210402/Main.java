package ac20210402;
import java.util.Scanner;

public class Main {
	static int[][] sum = new int[605][605], data = new int[605][605];
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt(), l = sc.nextInt(), r = sc.nextInt(), t = sc.nextInt();
		
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= n; j++)
				data[i][j] = sc.nextInt();
		
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= n; j++)
				sum[i][j] = sum[i-1][j] + sum[i][j-1] - sum[i-1][j-1] + data[i][j];
		
		int res = 0;		
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= n; j++) {
				int x1, x2, y1, y2;
				x1 = Math.max(1, i-r);
				x2 = Math.min(n, i+r);
				y1 = Math.max(1, j-r);
				y2 = Math.min(n, j+r);
				int tarSum = sum[x2][y2] - sum[x1-1][y2] - sum[x2][y1-1] + sum[x1-1][y1-1];
				if(tarSum <= t*(x2-x1+1)*(y2-y1+1)) res++;
			}
		System.out.println(res);				
	}
}
