package ac20210401;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt(), m =sc.nextInt(),L =sc.nextInt();
		int[] h = new int[L];
		for(int i = 0; i < m; i ++) {
			for(int j = 0; j < n; j++) {
				int num = sc.nextInt();
				h[num]++;
			}
		}
		for(int i : h) System.out.print(i + " ");	
	}
}
