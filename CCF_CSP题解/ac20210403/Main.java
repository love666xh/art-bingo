package ac20210403;
import java.util.Scanner;

public class Main {
	static final int N = 100010;
	static int n,m,t_def,t_max,t_min; // ip地址个数，请求个数，ip地址，ip期限
	static String h; // 主机名称
	static class Ip{
		int state; // 1.未分配，2.占用，3.过期
		int t; // 过期时间
		String owner;
		public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public int getT() {
            return t;
        }

        public void setT(int t) {
            this.t = t;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }
	}
	static Ip[] ip = new Ip[N];
	
	static void update_ips_state(int tc) {
		//对所有ip更新
	    for(int i=1; i<=n; i++){
	        //如果ip的过期时刻大于0，并且过期了
	        if(ip[i].getT() > 0 && ip[i].getT()<= tc){
	            //如果待分配，则状态变成未分配，且占用者清空，过期时刻清零
	            if(ip[i].getState() == 1){
	            	ip[i] = new Ip();
//	                ip[i].setState(0);
//	                ip[i].setOwner("");
//	                ip[i].owner="";
//	                ip[i].t=0;
	            }
	            //否则该地址的状态会由占用自动变为过期，且过期时刻清零。
	            else{
	            	ip[i].setState(3);
	            	ip[i].setT(0);
//	                ip[i].state=3;
//	                ip[i].t=0;
	            }
	        }
	    }
	}
	
	//选取特定状态的ip
	static int get_ip_by_state(int state)
	{
	    for(int i=1; i<=n; i++)
	        if(ip[i].getState() == state)
	            return i;
	    return 0; //否则返回分配失败
	}
	
	//通过client找一下有没有用过的ip
	static int get_ip_by_owner(String client)
	{
	    for(int i=1; i<=n; i++)
	        if(ip[i].getOwner() == client)
	            return i;
	    return 0;
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		n =sc.nextInt();t_def =sc.nextInt();t_max =sc.nextInt();t_min =sc.nextInt();h =sc.next();
		m =sc.nextInt();
		for(int i = 1; i <= n; i++) {
			ip[i] = new Ip();
		}
		while((m--) > 0) {
			int tc = sc.nextInt(); // 收到报文的时间
			//<发送主机> <接收主机> <报文类型> <IP 地址> <过期时刻>
			String client = sc.next(), server = sc.next(),type = sc.next();
			int id = sc.nextInt(), te = sc.nextInt(); 
			// 判断接收主机是否为本机，或者为 *，若不是，则判断类型是否为 Request，若不是，则不处理；
			if(server != h && server != "*") {
				if(type != "REQ") continue;
			}
			// 若类型不是 Discover、Request 之一，则不处理；
			if(type != "Discover" || type != "Request")continue;
			// 若接收主机为 *，但类型不是 Discover，或接收主机是本机，但类型是 Discover，则不处理。
			if(server == "*" && type != "Discover" || server == h && type == "Discover")continue;
			
			update_ips_state(tc); 
			
			//处理dis报文
	        if(type == "DIS")
	        {
	            //检查是否有占用者为发送主机的 IP 地址：
	            //若有，则选取该 IP 地址；
	            int k=get_ip_by_owner(client);
	            //若没有，则选取最小的状态为未分配的 IP 地址；
	            if(k==0) k=get_ip_by_state(0);//0是未分配
	            // 若没有，则选取最小的状态为过期的 IP 地址；
	            if(k==0) k=get_ip_by_state(3);//过期为3
	            // 若没有，则不处理该报文，处理结束
	            if(k==0) continue;
	            //当分配到ip地址以后
	            // 将该 IP 地址状态设置为待分配，占用者设置为发送主机；
//	            ip[k].state=1;ip[k].owner=client;
	            ip[k].setState(1);ip[k].setOwner(client);
	            // 若报文中过期时刻为 0 ，则设置过期时刻为 t+Tdef；
	            if(te==0)	ip[k].setT(tc+t_def);
	            // 否则根据报文中的过期时刻和收到报文的时刻计算过期时间，
	            // 判断是否超过上下限：若没有超过，则设置过期时刻为报文中
	            // 的过期时刻；否则则根据超限情况设置为允许的最早或最晚的过期时刻；
	            else{
	                int t= te- tc;//想使用的时间长度
	                t=Math.max(t, t_min);t=Math.min(t, t_max);
	                //t既不能小于最低时间，也不能超过最长时间
	                ip[k].setT(tc+t);
	            }
	            //应该发送报文了
	            System.out.println(h + " " + client + " " + "OFR" + " " + k + " " + ip[k].getT());
	        }
	        //处理request请求
	        else {
	            //如果接受者不是本机
	            if (server != h) {
	                // 到占用者为发送主机的所有 IP 地址，对于其中状态为待分配的，将其状态设置为未分配，并清空其占用者，清零其过期时刻，处理结束；
	                for(int i=1; i<=n; i++)
	                {
	                    if(ip[i].getOwner() == client && ip[i].getState() == 1)
	                    {
	                    	ip[i] = new Ip();
//	                        ip[i].state = 0;
//	                        ip[i].owner= "";
//	                        ip[i].t=0;
	                    }
	                }
	                continue;
	            }
	            // 检查报文中的 IP 地址是否在地址池内，若不是，则向发送主机发送 Nak 报文，处理结束；
	            if(!(id>=1 && id<=n && ip[id].getOwner() == client))
	            	System.out.println(h + " " + client + " " + "NAK" + " " + id + " " + 0);
	            //如果在地址池内
	            else{
	                // 无论该 IP 地址的状态为何，将该 IP 地址的状态设置为占用；
	                ip[id].setState(2);
//	            	ip[id].state=2;
	                // 与 Discover 报文相同的方法，设置 IP 地址的过期时刻；
	                if(te==0) ip[id].setT(tc+t_def);
	                else{
	                    int t= te- tc;//想使用的时间长度
	                    t=Math.max(t, t_min);t=Math.min(t, t_max);
	                    //t既不能小于最低时间，也不能超过最长时间
	                    ip[id].setT(tc+t);
//	                    ip[id].t=tc+t;
	                }
	                //应该发送报文了
	                System.out.println(h + " " + client + " " + "ACK" + " " + id + " " + ip[id].getT());
	            }
	         }
		}
	}
}
