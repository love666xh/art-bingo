package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class ConnectDatabase {

	public static Connection connectDB() {

		Properties properties = new Properties();

		try {
			properties.load(ConnectDatabase.class.getClassLoader().getResourceAsStream("database/mysql.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		String drivername = properties.getProperty("mysql.drivername");
		String url = properties.getProperty("mysql.url");
		String user = properties.getProperty("mysql.user");
		String password = properties.getProperty("mysql.password");

		Connection con = null;

		try {
			Class.forName(drivername);
			con = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return con;
	}
}
