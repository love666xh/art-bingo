# 【CCF-CSP】解题笔记

------

**作者：文艺倾年 ** **QQ：1531137510**

[TOC]

# 一、前言
该内容已开源上传至GitHub，欢迎大佬指正！
# 二、传送门

[个人博客](https://blog.csdn.net/m0_51517236?spm=1011.2124.3001.5343 )

 [文艺倾年 - 博客园 ](https://www.cnblogs.com/itxh-lover/) 

[CCF-CSP官网](https://cspro.org/)

Github主页

# 三、打怪升级图
| 题目编号 |        题目        |           分类           |
| :------: | :----------------: | :----------------------: |
| 201312-1 |  出现次数最多的数  |         数组水题         |
| 201312-2 |     ISBN 号码      |        字符串处理        |
| 201312-3 |     最大的矩形     |         暴力枚举         |
| 201312-4 |      有趣的数      |         动态规划         |
| 201312-5 |     I’m stuck      |           BFS            |
| 201403-1 |       相反数       |         暴力枚举         |
| 201403-2 |        窗口        |         STL 应用         |
| 201403-3 |     命令行选项     |           模拟           |
| 201403-4 |      无线网络      |          最短路          |
| 201403-5 |      任务调度      |         动态规划         |
| 201409-1 |      相邻数对      |         暴力枚举         |
| 201409-2 |        画图        |          散列表          |
| 201409-3 |     字符串匹配     |           模拟           |
| 201409-4 |      最优配餐      |            图            |
| 201409-5 |        拼图        | 动态规划、矩阵乘法快速幂 |
| 201412-1 |      门禁系统      |          散列表          |
| 201412-2 |     Z 字形扫描     |         STL 应用         |
| 201412-3 |      集合竞价      |           枚举           |
| 201412-4 |      最优灌溉      |           图论           |
| 201412-5 |      货物调度      |           图论           |
| 201503-1 |      图像旋转      |         STL 应用         |
| 201503-2 |      数字排序      |          散列表          |
| 201503-3 |        节日        |           模拟           |
| 201503-4 |      网络延时      |         BFS/DFS          |
| 201503-5 |      最小花费      |            树            |
| 201509-1 |      数列分段      |         STL 应用         |
| 201509-2 |      日期计算      |           打表           |
| 201509-3 |    模板生成系统    |           模拟           |
| 201509-4 |      高速公路      |                          |
| 201509-5 |      最佳文章      |                          |
| 201512-1 |      数位之和      |         STL 应用         |
| 201512-2 |     消除类游戏     |           水题           |
| 201512-3 |        画图        |         图形输出         |
| 201512-4 |        送货        |         欧拉路径         |
| 201512-5 |        矩阵        |        矩阵快速幂        |
| 201604-1 |      折点计数      |         STL 应用         |
| 201604-2 |     俄罗斯方块     |           模拟           |
| 201604-3 |      路径解析      |        字符串处理        |
| 201604-4 |        游戏        |          三维DP          |
| 201604-5 |      网络连接      |         斯坦纳树         |
| 201609-1 |      最大波动      |           水题           |
| 201609-2 |      火车购票      |         STL 应用         |
| 201609-3 |      炉石传说      |           模拟           |
| 201609-4 |      交通规划      |        单源最短路        |
| 201609-5 |        祭坛        |      离散化+线段树       |
| 201612-1 |       中间数       |         STL 应用         |
| 201612-2 |      工资计算      |           打表           |
| 201612-3 |      权限查询      |          散列表          |
| 201612-4 |      压缩编码      |        哈夫曼编码        |
| 201612-5 |      卡牌游戏      |         概率博弈         |
| 201703-1 |       分蛋糕       |           水题           |
| 201703-2 |      学生排队      |         STL 应用         |
| 201703-3 |      Markdown      |           模拟           |
| 201703-4 |      地铁修建      |           图论           |
| 201703-5 |      引水入城      |           图论           |
| 201709-1 |       打酱油       |           水题           |
| 201709-2 |     公共钥匙盒     |           模拟           |
| 201709-3 |     JSON 查询      |                          |
| 201709-4 |      通信网络      |           图论           |
| 201709-5 |        除法        |                          |
| 201712-1 |      最小差值      |         STL 应用         |
| 201712-2 |        游戏        |         STL 应用         |
| 201712-3 |      Crontab       |                          |
| 201712-4 |      行车路线      |                          |
| 201712-5 |        商路        |                          |
| 201803-1 |       跳一跳       |           水题           |
| 201803-2 |     碰撞的小球     |          散列表          |
| 201803-3 |      URL 映射      |           模拟           |
| 201803-4 |      棋局评估      |                          |
| 201803-5 |      二次求和      |                          |
| 201809-1 |        卖菜        |                          |
| 201809-2 |        买菜        |         暴力枚举         |
| 201809-3 |     元素选择器     |                          |
| 201809-4 |       再卖菜       |                          |
| 201809-5 |     线性递推式     |                          |
| 201812-1 |      小明上学      |           水题           |
| 201812-2 |      小明放学      |           水题           |
| 201812-3 |     CIDR 合并      |                          |
| 201812-4 |      数据中心      |                          |
| 201812-5 |      管道清洁      |                          |
| 201903-1 |       小中大       |           水题           |
| 201903-2 |      二十四点      |         STL 应用         |
| 201903-3 |    损坏的 RAID5    |           模拟           |
| 201903-4 |    消息传递接口    |           模拟           |
| 201903-5 |    317 号子任务    |                          |
| 201909-1 |     小明种苹果     |           水题           |
| 201909-2 |  小明种苹果（续）  |         STL 应用         |
| 201909-3 |       字符画       |           模拟           |
| 201909-4 |      推荐系统      |         STL 应用         |
| 201909-5 |      城市规划      |                          |
| 201912-1 |        报数        |         STL 应用         |
| 201912-2 |     回收站选址     |          散列表          |
| 201912-3 |     化学方程式     |           模拟           |
| 201912-4 |       区块链       |           模拟           |
| 201912-5 |        魔数        |                          |
| 202006-1 |     线性分类器     |           数学           |
| 202006-2 |      稀疏向量      |          散列表          |
| 202006-3 |  Markdown 渲染器   |                          |
| 202006-4 |        1246        |                          |
| 202006-5 |  乔乔和牛牛逛超市  |                          |
| 202009-1 |    称检测点查询    |           水题           |
| 202009-2 |    风险人群筛查    |           水题           |
| 202009-3 |    点亮数字人生    |           模拟           |
| 202009-4 |      星际旅行      |                          |
| 202009-5 |     密信与计数     |                          |
| 202012-1 | 期末预测之安全指数 |           水题           |
| 202012-2 | 期末预测之最佳阈值 |         STL 应用         |
| 202012-3 |  带配额的文件系统  |           模拟           |
| 202012-4 |      食材运输      |         动态规划         |
| 202012-5 |      星际旅行      |          线段树          |
| 202104-1 |     灰度直方图     |          散列表          |
| 202104-2 |      邻域均值      |        二维前缀和        |
| 202104-3 |    DHCP 服务器     |                          |
| 202104-4 |     校门外的树     |         动态规划         |
| 202104-5 |      疫苗运输      |                          |

# 四、装备
[Java基础知识题库笔记](https://blog.csdn.net/m0_51517236/article/details/119815957?spm=1001.2014.3001.5501)
## 1.常用包
```java
import java.util.Scanner;
import java.util.Map;
import java.util.Collection;
import java.lang.Math;
```
## 2.输入输出
```java
import java.util.Scanner; 
Scanner sc = new Scanner(System.in);
```
```java
// 字符串
String s = sc.next();
String str = sc.nextLine();  // 读取一行，含空格

// 数字
int n = sc.nextInt();
while(sc.hasNext()){ // 控制台手工输入ctrl+z退出
	int a=input.nextInt();
}
```
## 3.字符串
```java
str.length(); // 字符串（有括号）

nums.length; // 数组（无括号）
```
常用函数：
```java
int a = Integer.parseInt(str); // 字符串→数字
String s = String.valueOf(value); // Other→字符串

s = str.substring(0,1);  // (,] 范围
int s =str.indexOf("文艺倾年"); // 检索字符串
char s = str.charAt(index); // 访问某个字符

a.equals(b) // 检测字符串相等  不能 == 

String[] strs = str.split(" "); // 字符串分割

str.replace("文艺倾年","小航"); // 字符串替换

str.trim(); // 去除头尾空格
```
## 4.数组
```java
Arrays.fill(nums,0); // 填充数组为0

Arrays.sort(nums); // 升序
```
## 5.ArrayList
```java
Collections.fill(list,0); // 填充数组为0

// 增删
list.add(1);
list.add(3,4); // 将4插入到第三个位置
list.remove(3); // 删除下标为3的

Collections.sort(list); // 排序
Collections.max(list); // 最大值
Collections.min(list);
Collections.shuffle(list);  //list洗牌

Collections.reverse(list); // 翻转

list.toArray() // list变数组

list.isEmpty() //list是否为空
```
## 6.HashMap
```java
map.put(a,b);  // 插入

// 查找
if(map.containsKey(k));
if(map.containsvalue(v));

int value = map.get(k); // 取值

// 遍历
Iterator<Integer> it = map.keySet().iterator();
while(it.hasNext()){
	Integer key=it.next();
	Integer value=map.get(key);
}

// 提取其中所有key
for(Integer key:map.keySet()){
	...
}
```
## 7.HashSet
```java
set.add(a); // 插入
set.remove(b); // 删除
set.contains(a); // 查询
set.remove(a); // 移除
set.clear(); // 清除
```
## 8.LInkedList
```java
LinkedList<Pet> pets = new LinkedList<Pet>(Pet.arrayList(5));//生成五个Pet对象

// 取第一个
pets.getFirst() // 列表为空返回NoSuchElement-Exception
pets.element() // 列表为空返回NoSuchElement-Exception
pets.peek() // 列表为空返回null

// 移除第一个，并返回列表头
pets.removeFirst() // 列表为空返回NoSuchElement-Exception
pets.remove() // 列表为空返回NoSuchElement-Exception
pets.poll() // 列表为空返回null

pets.addFirst(new Rat()); // 插入头部

// 插入尾部
pets.addLast(new Rat()); 
pets.add(new Rat()); 
pets.offer(new Rat()); 

 pets.set(2,new Rat());//将替换为指定的元素
```

# 五、题解

## 202104-1 灰度直方图

| 试题编号： |  202104-1  |
| :--------: | :--------: |
| 试题名称： | 灰度直方图 |
| 时间限制： |    1.0s    |
| 内存限制： |  512.0MB   |

题目内容：

**问题描述**
  一幅长宽分别为 n 个像素和 m 个像素的灰度图像可以表示为一个 ==n×m 大小的矩阵A==。其中每个元素 ==Aij==（0≤i<n、0≤j<m）是一个  ==[0,L)== 范围内的整数，表示对应位置像素的灰度值。具体来说，一个 8 比特的灰度图像中每个像素的灰度范围是 [0,128)。
  一副灰度图像的灰度统计直方图（以下简称“直方图”）可以表示为一个==长度为 L==的数组 h，其中 ==h[x]==（0≤x<L）表示该图像中灰度值为 ==x 的像素个数==。显然，h[0] 到 h[L−1] 的总和应等于图像中的像素总数 n⋅m。
  已知一副图像的灰度矩阵 A，试计算其灰度直方图 h[0],h[1],⋯,h[L−1]。

	人话就是：打印矩阵内每种数字出现的次数

**输入格式**
　　输入共 n+1 行。
  输入的第一行包含三个用空格分隔的正整数 n、m 和 L，含义如前文所述。
  第二到第 n+1 行输入矩阵 A。第 i+2（0≤i<n）行包含用空格分隔的 m 个整数，依次为 Ai0,Ai1,⋯,Ai(m−1)。

**输出格式**
　　输出仅一行，包含用空格分隔的 L 个整数 h[0],h[1],⋯,h[L-1]，表示输入图像的灰度直方图。
　　
**样例输入**

	4 4 16
	0 1 2 3
	4 5 6 7
	8 9 10 11
	12 13 14 15

**样例输出**

	1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1

**样例输入**

	7 11 8
	0 7 0 0 0 7 0 0 7 7 0
	7 0 7 0 7 0 7 0 7 0 7
	7 0 0 0 7 0 0 0 7 0 7
	7 0 0 0 0 7 0 0 7 7 0
	7 0 0 0 0 0 7 0 7 0 0
	7 0 7 0 7 0 7 0 7 0 0
	0 7 0 0 0 7 0 0 7 0 0

**样例输出**

	48 0 0 0 0 0 0 29

>0出现了48次，7出现了29次，1-6出现了0次

**数据规模和约定**

	全部的测试数据满足 0<n,m≤500 且 4≤L≤256。

题型：`散列表`

AC代码：
```java
import java.util.Scanner;
public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt(), m =sc.nextInt(),L =sc.nextInt();
		int[] h = new int[L];
		for(int i = 0; i < m; i ++) {
			for(int j = 0; j < n; j++) {
				int num = sc.nextInt();
				h[num]++;
			}
		}
		for(int i : h) System.out.print(i + " ");	
	}
}
```

## 202104-2 邻域均值
| 试题编号： | 202104-2 |
| ---------- | -------- |
| 试题名称： | 邻域均值 |
| 时间限制： | 1.0s     |
| 内存限制： | 512.0MB  |
题目内容：

**试题背景**

顿顿在学习了数字图像处理后，想要对手上的一副灰度图像进行降噪处理。不过该图像仅在较暗区域有很多噪点，如果贸然对全图进行降噪，会在抹去噪点的同时也模糊了原有图像。因此顿顿打算先使用**邻域均值**来判断一个像素是否处于**较暗区域**，然后仅对处于**较暗区域**的像素进行降噪处理。

**问题描述**

待处理的灰度图像长宽皆为 n 个像素，可以表示为一个 ==n×n== 大小的矩阵 A，其中每个元素是一个 ==[0,L)== 范围内的整数，表示对应位置像素的灰度值。
对于矩阵中任意一个元素 Aij（0≤i,j<n），其**邻域**定义为附近若干元素的集和：

==Neighbor(i,j,r)={Axy|0≤x,y<n and |x−i|≤r and |y−j|≤r}==

这里使用了一个额外的参数 r 来指明 Aij 附近元素的具体范围。根据定义，易知 Neighbor(i,j,r) 最多有 (2r+1)2 个元素。

如果元素 Aij **邻域**中所有元素的**平均值**==小于或等于==一个给定的阈值 t，我们就认为该元素对应位置的像素处于**较暗区域**。
下图给出了两个例子，左侧图像的较暗区域在右侧图像中展示为黑色，其余区域展示为白色。

![在这里插入图片描述](https://img-blog.csdnimg.cn/68b781a5818044c08538d3480db75316.png)

现给定邻域参数 r 和阈值 t，试统计输入灰度图像中有多少像素处于**较暗区域**。

**输入格式**

输入共 n+1 行。

输入的第一行包含四个用空格分隔的正整数 n、L、r 和 t，含义如前文所述。

第二到第 n+1 行输入矩阵 A。
第 i+2（0≤i<n）行包含用空格分隔的 n 个整数，依次为 Ai0,Ai1,⋯,Ai(n−1)。

**输出格式**

输出一个整数，表示输入灰度图像中处于较暗区域的像素总数。

**样例输入**

```data
4 16 1 6
0 1 2 3
4 5 6 7
8 9 10 11
12 13 14 15
```
**样例输出**

```data
7
```

**样例输入**

```data
11 8 2 2
0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0
0 7 0 0 0 7 0 0 7 7 0
7 0 7 0 7 0 7 0 7 0 7
7 0 0 0 7 0 0 0 7 0 7
7 0 0 0 0 7 0 0 7 7 0
7 0 0 0 0 0 7 0 7 0 0
7 0 7 0 7 0 7 0 7 0 0
0 7 0 0 0 7 0 0 7 0 0
0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0
```
**样例输出**

```data
83
```

**评测用例规模与约定**

```data
70% 的测试数据满足 n≤100、r≤10。

全部的测试数据满足 0<n≤600、0<r≤100 且 2≤t<L≤256。
```
解题思路：`二维前缀和`
手写图解：

<div align="center">
    	<img src="https://img-blog.csdnimg.cn/87ecab73fdcb496692a2c342d04849c2.png">  
</div>

公式解读：
$$
|x-i|\ \le \ r
$$
$$
i-r\ \le \ x\ \le \ i+r
$$
$$
1\ \le \ x\ \le \ n
$$
$$
x1\ \in \ \max \left( 1,i-r \right)
$$
$$
x2\ \in \ \min \left( n,i+r \right)
$$

二维前缀和（运用了容斥定理）图解:

![在这里插入图片描述](https://img-blog.csdnimg.cn/ad920084c7c644f7be06255e2546df82.png)
$$
S\left( x_2,y_2 \right) =S\left( x_2,y_1 \right) +S\left( x_1,y_2 \right) -S\left( x_1,y_1 \right) +A\left( x_2,y_2 \right)
$$
$$
A\left( x_2,y_2 \right) =S\left( x_2,y_2 \right) -S\left( x_2,y_1 \right) -S\left( x_1,y_2 \right) +S\left( x_1,y_1 \right)
$$
【补充】

设b[]为前缀和数组，a[]为原数组，根据这句话可以得到前缀和的定义式和递推式：

|            | 定义式                                                       | 递推式                                                       |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 一维前缀和 | ![b[i]=\sum_{j=0}^{i}a[j]](https://private.codecogs.com/gif.latex?b%5Bi%5D%3D%5Csum_%7Bj%3D0%7D%5E%7Bi%7Da%5Bj%5D) | ![b[i]=b[i-1]+a[i]](https://private.codecogs.com/gif.latex?b%5Bi%5D%3Db%5Bi-1%5D&plus;a%5Bi%5D) |
| 二维前缀和 | ![b[x][y]=\sum_{i=0}^{x}\sum_{j=0}^{y}a[i][j]](https://private.codecogs.com/gif.latex?b%5Bx%5D%5By%5D%3D%5Csum_%7Bi%3D0%7D%5E%7Bx%7D%5Csum_%7Bj%3D0%7D%5E%7By%7Da%5Bi%5D%5Bj%5D) | ![b[x][y]=b[x-1][y]+b[x][y-1]-b[x-1][y-1]+a[x][y]](https://private.codecogs.com/gif.latex?b%5Bx%5D%5By%5D%3Db%5Bx-1%5D%5By%5D&plus;b%5Bx%5D%5By-1%5D-b%5Bx-1%5D%5By-1%5D&plus;a%5Bx%5D%5By%5D) |
【差分】
差分是一个数组相邻两元素的差，一般为下标靠后的减去靠前的一个。设差分数组p[]，即：

![p[i]=a[i]-a[i-1]](https://private.codecogs.com/gif.latex?p%5Bi%5D%3Da%5Bi%5D-a%5Bi-1%5D)
【联系】
 `前缀和` 和 `差分` 是一对`互逆`过程。
AC代码：

```java
import java.util.Scanner;

public class Main {
	static int[][] sum = new int[605][605], data = new int[605][605];
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt(), l = sc.nextInt(), r = sc.nextInt(), t = sc.nextInt();
		
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= n; j++)
				data[i][j] = sc.nextInt();
		
		// 前缀和
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= n; j++)
				sum[i][j] = sum[i-1][j] + sum[i][j-1] - sum[i-1][j-1] + data[i][j];
		
		int res = 0;		
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= n; j++) {
				int x1, x2, y1, y2;
				x1 = Math.max(1, i-r);
				x2 = Math.min(n, i+r);
				y1 = Math.max(1, j-r);
				y2 = Math.min(n, j+r);
				// 通过前缀和拿到区域面积
				int tarSum = sum[x2][y2] - sum[x1-1][y2] - sum[x2][y1-1] + sum[x1-1][y1-1];
				if(tarSum <= t*(x2-x1+1)*(y2-y1+1)) res++;
			}
		System.out.println(res);				
	}
}
```

